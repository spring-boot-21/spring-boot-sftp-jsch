package com.zademy.upload.files.sftp.utils;

import java.io.InputStream;

/**
 * The Interface SFTPUtils.
 */
public interface SFTPUtils {

	/**
	 * Upload file to SFTP.
	 *
	 * @param filename       the filename
	 * @param fis            the fis
	 * @param destinationDir the destination dir
	 * @return the string
	 */
	String uploadFileToSFTP(String filename, InputStream fis, String destinationDir);

	/**
	 * Check exist.
	 *
	 * @param fileName       the file name
	 * @param destinationDir the destination dir
	 * @return true, if successful
	 */
	boolean checkExist(String fileName, String destinationDir);

	/**
	 * Delete file.
	 *
	 * @param fileName       the file name
	 * @param destinationDir the destination dir
	 */
	void deleteFile(String fileName, String destinationDir);

}
