package com.zademy.upload.files.sftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.zademy.upload.files.sftp.utils.SFTPUtils;

/**
 * The Class Application.
 */
@SpringBootApplication
public class Application implements CommandLineRunner {

	/** The logger. */
	private Logger logger = LoggerFactory.getLogger(Application.class);

	/** The sftp utils. */
	@Autowired
	private SFTPUtils sftpUtils;

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	/**
	 * Run.
	 *
	 * @param args the args
	 * @throws Exception the exception
	 */
	@Override
	public void run(String... args) throws Exception {

		String destinationDir = "/home/centos/Temporales";

		// Get File
		File file = new File("C:\\aow_drv.log");

		// Get File Imagen
		File fileImage = new File("C:\\wallpapers.jpg");

		FileInputStream fin;

		FileInputStream finImage;

		String result = "";

		try {

			// Upload File
			fin = new FileInputStream(file);

			result = sftpUtils.uploadFileToSFTP("aow_drv.log", fin, destinationDir);

			logger.info("File upload: {}", result);

			// Upload Image
			finImage = new FileInputStream(fileImage);

			result = sftpUtils.uploadFileToSFTP("wallpapers.jpg", finImage, destinationDir);

			logger.info("Image upload: {}", result);
			
			//Exist File
			if(sftpUtils.checkExist("wallpapers.jpg", destinationDir)) {
				
				logger.info("File Exist -> wallpapers.jpg");
				
			}
			
			// Delete File
			sftpUtils.deleteFile("aow_drv.log", destinationDir);

		} catch (FileNotFoundException | SecurityException | NullPointerException e) {
			logger.error("File not found", e);
		}

	}

}
