package com.zademy.upload.files.sftp.utils.impl;

import java.io.InputStream;
import java.util.Properties;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;
import com.zademy.upload.files.sftp.utils.SFTPUtils;

/**
 * The Class SFTPUtilsImpl.
 */
@Component
public class SFTPUtilsImpl implements SFTPUtils {

	/** The logger. */
	private Logger logger = LoggerFactory.getLogger(SFTPUtilsImpl.class);

	/** The host. */
	@Value("${sftp.connection.host}")
	private String host;

	/** The port. */
	@Value("${sftp.connection.port}")
	private Integer port;

	/** The user. */
	@Value("${sftp.connection.user}")
	private String user;

	/** The password. */
	@Value("${sftp.connection.password}")
	private String password;

	/** The channel sftp. */
	private ChannelSftp channelSftp;

	/** The channel. */
	private Channel channel;

	/** The session. */
	private Session session;

	/**
	 * Instantiates a new SFTP utils.
	 */
	public SFTPUtilsImpl() {
		super();
	}

	/**
	 * Inits the session.
	 */
	private void initSession() {

		try {

			JSch jsch = new JSch();

			logger.info("host: {}", host);
			logger.info("port: {}", port);
			logger.info("user: {}", user);
			logger.info("pass: {}", password);

			session = jsch.getSession(user, host, port);

			session.setPassword(password);

			Properties config = new Properties();

			config.put("StrictHostKeyChecking", "no");

			session.setConfig(config);

		} catch (Exception ex) {

			logger.error("Error", ex);

		}

	}

	/**
	 * Upload file to FTP.
	 *
	 * @param filename       the filename
	 * @param fis            the fis
	 * @param destinationDir the destination dir
	 * @return the string
	 */
	public String uploadFileToSFTP(String filename, InputStream fis,  String destinationDir) {

		String result = "";

		initSession();

		try {

			if (!session.isConnected()) {
				
				session.connect();
				
			}	

			channel = session.openChannel("sftp");

			channel.connect();

			channelSftp = (ChannelSftp) channel;

			validateDirectory(destinationDir);

			channelSftp.put(fis, filename);

			logger.info("Upload successful portfolio file name: {}", filename);
			result = String.format("sftp://%s/%s/%s", host, destinationDir, filename);

			channelSftp.exit();
			channel.disconnect();
			session.disconnect();

		} catch (SftpException | JSchException e) {

			logger.error("Error -> ", e);

		}

		return result;
	}

	/**
	 * Check exist.
	 *
	 * @param fileName       the file name
	 * @param destinationDir the destination dir
	 * @return true, if successful
	 */
	public boolean checkExist(String fileName, String destinationDir) {

		boolean existed = false;

		initSession();

		try {

			if (!session.isConnected()) {
				
				session.connect();
				
			}	

			channel = session.openChannel("sftp");
			channel.connect();
			channelSftp = (ChannelSftp) channel;

			validateDirectory(destinationDir);

			Vector ls = channelSftp.ls(destinationDir);

			if (ls != null) {

				// Iterate listing.
				logger.info(fileName);

				for (int i = 0; i < ls.size(); i++) {

					LsEntry entry = (LsEntry) ls.elementAt(i);
					
					String fileNameAux = entry.getFilename();

					if (!entry.getAttrs().isDir()) {

						if (fileName.toLowerCase().startsWith(fileNameAux)) {
							existed = true;
						}

					}

				}

			}

			channelSftp.exit();
			channel.disconnect();
			session.disconnect();

		} catch (SftpException | JSchException e) {

			existed = false;

			if (session.isConnected()) {

				session.disconnect();

			}

		}

		return existed;
	}

	/**
	 * Delete file.
	 *
	 * @param fileName       the file name
	 * @param destinationDir the destination dir
	 */
	public void deleteFile(String fileName, String destinationDir) {

		initSession();

		try {

			if (!session.isConnected()) {
				
				session.connect();
				
			}	

			channel = session.openChannel("sftp");
			channel.connect();
			
			channelSftp = (ChannelSftp) channel;
			channelSftp.cd(destinationDir);
			channelSftp.rm(fileName);
			
			logger.info("File Delete");
			
			channelSftp.exit();
			
			channel.disconnect();
			session.disconnect();

		} catch (SftpException | JSchException e) {

			logger.info(e.getMessage());

			if (session.isConnected()) {
				
				session.disconnect();
				
			}

		}

	}

	/**
	 * Validate directory.
	 *
	 * @param destinationDir the destination dir
	 * @throws SftpException the sftp exception
	 */
	private void validateDirectory(String destinationDir) throws SftpException {

		try {

			channelSftp.cd(destinationDir);

			logger.info("cd relative Dir");

		} catch (SftpException e) {
			
			//If the directory is not located, it is created
			channelSftp.mkdir(destinationDir);
			channelSftp.cd(destinationDir);

		}

	}

}
